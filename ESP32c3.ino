/*
Code Authored by Ahmed Abdellatif use without explicit written consent is not allowed 
*/

#define mPin A0 // Vibration Motor Pin
#define pPin A1 // Air Pump Pin
#define vPin A2 // Valve Pin


int mDelay = 0;
int aDuration = 0; // Air Pumping Duration
int intensity = 0; // Intensity mode used to set the vibration intensity and air pumping intensity 

int data [3] ; // Array to store the different modes
int inData = 000; // for incoming serial data

int mMode = 0; //Motor Mode
int iMode = 0; //Intensity Mode
int aMode = 0; //Air Mode


void setup() {
  // Open serial communication:
  Serial.begin(9600);
  data [0] = 0; 
  data [1] = 0; 
  data [2] = 0;
  pinMode(mPin, OUTPUT); // setting vibration motors off as default 
  pinMode(pPin, OUTPUT); //air pump off as default 
  pinMode(vPin, OUTPUT); //valve off as default 
}

void loop() {
  // Read serial input:
  if (Serial.available() > 0) {
  parseData(); 
  setModes();
  Serial.flush();
}

  // If the motor mode is not Zero as sent by the interface board
if (mMode!=0){
actuate();
}else{
  analogWrite(mPin, 0);
  analogWrite(pPin, 0);
  analogWrite(vPin, 0);
}




}


  // Function to parse the data coming through serial 
void parseData() {
    // read the incoming byte and parse it as int value 
    inData=Serial.parseInt();
    // say what you got:
    //Serial.println("I received: ");
    //Serial.println(inData);
  data [0] = (inData/100); //iMode
  iMode = data [0];
  data [1] = ((inData-(data [0]*100))/10); //aMode
  aMode = data [1];
  data [2] = inData-((data [0]*100)+(data [1]*10)); //mMode
  mMode = data [2]; 
  /*for(int i = 0; i < 3; i++)
{
  Serial.print(data[i]);
}
  Serial.println("");*/
}

  // Function set the different modes  
void setModes() {
switch (mMode) { // setting the vibration motor mode 
    case 1:  // motor mode 1
      mDelay = 3000;
      break;
    case 2:  // motor mode 2
      mDelay = 1500;
      break;
    case 3:  // motor mode 3
      mDelay = 100;
      break;
    default:
      mMode = 0;
      break;
  }
  
  //Serial.println(mDelay);

 switch (aMode) { // setting the air mode 
    case 1:  // air mode 1
      aDuration = 400;
      break;
    case 2:  // air mode 2
      aDuration = 700;
      break;
    default:
      aDuration = 0;
      break;
  }
  
  //Serial.println(aDuration);
 

switch (iMode) { // setting the intensity mode
    case 1:  // intensity level 1
      intensity = 155;
      break;
    case 2:  // intensity level 2
      intensity = 180;
      break;
    case 3:  // intensity level 3
      intensity = 205;
      break;
    case 4:  // intensity level 4
      intensity = 230;
      break;
    case 5:  // intensity level 5
      intensity = 255;
      break;
    default:
      intensity = 0;
      break;  
      
  }

  //Serial.println(intensity);

  }



  // Function to actuate the vibration motors, inflation pump and the valve  
void actuate() { // Actuate vibration motors 
analogWrite(mPin, intensity);
analogWrite(pPin, intensity);
analogWrite(vPin, 0);  // valve code
delay (mDelay);
delay (aDuration);
//Serial.println(intensity);
analogWrite(mPin, 0);
analogWrite(pPin, 0);
analogWrite(vPin, 255);  // valve code
delay (mDelay);
delay (aDuration);
//Serial.println("off");

}

/*void inflate() { // Actuate vibration motors 
analogWrite(pPin, intensity);
delay (aDuration);
//Serial.println(intensity);
analogWrite(pPin, 0);
delay (aDuration);/*
//Serial.println("off");

}*/
